package com.infogain.jdbclogappender;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {
	
	private final Logger logger = LogManager.getLogger(getClass());
	
	@GetMapping(value = "log", produces = MediaType.APPLICATION_JSON_VALUE)
	public String generateReport() {

		logger.error("We are logging this in DB its ERROR level log");
		logger.info("We are logging this in DB its ERROR level log");
		
		return "Ok";
	}
	
}
