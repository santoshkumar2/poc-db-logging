package com.infogain.jdbclogappender;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

public class ConnectionFactory {
 private static interface Singleton {
  final ConnectionFactory INSTANCE = new ConnectionFactory();
 }

 private final DataSource dataSource;

 private ConnectionFactory() {
  Properties properties = new Properties();
  properties.setProperty("user", "root");
  properties.setProperty("password", "tiger");

  GenericObjectPool pool = new GenericObjectPool();
  DriverManagerConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
    "jdbc:mysql://127.0.0.1/test", properties);
  new PoolableConnectionFactory(connectionFactory, pool, null,
    "SELECT 1", 3, false, false,
    Connection.TRANSACTION_READ_COMMITTED);

  this.dataSource = new PoolingDataSource(pool);
 }

 public static Connection getDatabaseConnection() throws SQLException {
  System.out.print("=========== >>> Connection to db"); 
  return Singleton.INSTANCE.dataSource.getConnection();
 }
 public static String getConnection() throws SQLException {
	  System.out.print("=========== >>> HELLO");
	  return "";
	  //return Singleton.INSTANCE.dataSource.getConnection();
 }
}







