package com.infogain.jdbclogappender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocDbLoggingApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocDbLoggingApplication.class, args);
	}

}
